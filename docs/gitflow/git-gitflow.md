# Gitflow

[Return](../../README.md) to main page.

## Spis treści

- [Rodzaje gałęzi](#user-content-rodzaje-branchy)
- [Branch `feature/task`](#user-content-featuretask-branch)
- [Branch `hotfix/version`](#`hotfix/version`-branch)
- [`release/version` branch](#`release/version`-branch)
- [W przypadku braku gałęzi `release/version`. Gałąź `develop`](#W-przypadku-braku-gałęzi-`release/version`-Gałąź-`develop`)

## Rodzaje branchy
Wyróżniamy dwa rodzaje branchy:

Podstawowe: `master`, `develop`

Ulotne: `feature/task`, `release/version`, `hotfix/version`

## `feature/task` branch
Tworzymy ją z gałęzi `develop`
```bash
On branch develop
$ git branch feature/task
$ git checkout feature/task
```
lub
```bash
On branch develop
$ git checkout -b feature/task
```
Wykonujemy task, a na koniec rebase'ujemy 
```bash
On branch feature/task
$ git rebase -i HEAD~4
```
i mergujemy do `develop`
```bash
On branch feature/task
$ git checkout develop
$ git merge --no-ff feature/task
```
Gałąź `feature/task` usuwamy
```bash
On branch develop
$ git branch -D feature/task
```

## `hotfix/version` branch
Tworzymy ją z gałęzi `master`
```bash
On branch master
$ git branch hotfix/version
$ git checkout hotfix/version
```
lub
```bash
On branch master
$ git checkout -b hotfix/version
```
Naprawiamy bug w aplikacji, a następnie mergujemy ją do gałęzi `master` i `develop`
```bash
On branch hotfix/version
$ git checkout master
$ git merge --no-ff hotfix/version

On branch hotfix/version
$ git checkout develop
$ git merge --no-ff hotfix/version
```
Gałąź `master` tagujemy
```bash
On branch hotfix/version
$ git checkout master
$ git tag v0.1
```
Gałąź `hotfix/version` usuwamy
```bash
On branch develop
$ git branch -D hotfix/version
```

## `release/version` branch
Tworzymy ją z gałęzi `develop` w momencie gdy uznamy, że mamy na tyle tasków by wydać nową wersję.
```bash
On branch develop
$ git branch release/version
$ git checkout release/version
```
lub
```bash
On branch develop
$ git checkout -b release/version
```
Aktualizujemy dokumentację, wykonujemy małe poprawki. Mergujemy ją do gałęzi `master` i `develop`
```bash
On branch release/version
$ git checkout master
$ git merge --no-ff release/version

On branch release/version
$ git checkout develop
$ git merge --no-ff release/version
```
Gałąź `master` tagujemy
```bash
On branch release/version
$ git checkout master
$ git tag v0.1
```
Gałąź `release/version` usuwamy
```bash
On branch develop
$ git branch -D release/version
```

## W przypadku braku gałęzi `release/version`. Gałąź `develop`
Kiedy mamy na tyle tasków, że chcemy wydać nową wersję, mergujemy do gałęzi `master`
```bash
On branch develop
$ git checkout master
$ git merge --no-ff develop
```
Gałąź `master` tagujemy
```bash
On branch develop
$ git checkout master
$ git tag v0.1
```
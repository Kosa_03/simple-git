# GiT version control

This repository contains useful instructions.

## Commands
- [Ignoring files](docs/commands/git-ignoring-files.md)
- [Fixup older commit](docs/commands/git-fixup-older-commit.md)

## Gitflow
- [Git gitflow](docs/gitflow/git-gitflow.md)

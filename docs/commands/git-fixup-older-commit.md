# GiT useful commands

[Return](../../README.md) to main page.

## Fixup older commit
If we need change older commit than last on current branch, we can create new commit with changes we need and add `fixup` flag with older commit's hash.
```bash
git commit --fixup=<old_commit_hash>
```
In our case
```bash
git commit --fixup=26e826e
```
Next we need to make `git rebase`. Below I described two different ways.

### Interactive rebase
When we make this command
```bash
git rebase --interactive HEAD~3
```
Now we can see window with similar output like this
```bash
pick    26e826e     Added file-A.txt, file-B.txt
pick    e91187f     Added file-C.txt
pick    b70bbcc     fixup! Update file-A.txt
```
Move your `fixup` commit after commit which we want to change and add `squash` or `fixup` flag.
```bash
pick    26e826e     Added file-A.txt, file-B.txt
squash  b70bbcc     fixup! Update file-A.txt
pick    e91187f     Added file-C.txt
```
Save first rebase file. In next window set new commit message and that's it!

### Interactive rebase with --autosquash
When we make this command
```bash
git rebase --interactive --autosquash HEAD~3
```
In this case we can see window with similar output like this
```bash
pick    26e826e     Added file-A.txt, file-B.txt
fixup   b70bbcc     fixup! Update file-A.txt
pick    e91187f     Added file-C.txt
```
Save this rebase file and we're done!

### Addition

We can configure git to automatic squash, just set this option to `true`.
```bash
git config rebase.autosquash true
```
And now we dont need remember about `--autosquash` option during rebase.

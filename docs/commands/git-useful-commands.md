# GiT - Pomocne komendy

[Powrót](../README.md) do strony głównej.

## Spis treści

[Fixup starszego commitu](#poprawka-starszego-commitu)



## Cofanie zmian wykonanych za pomocą 'git rebase -i'

`git reset --hard ORIG_HEAD`


## Wycofanie do commita (bez zmian w strukturze plików)

`git reset <commit>`


## Zmiana parent commita dla gałęzi

`git rebase --onto <new parent> <old parent>`

```bash
HEAD~1      # jeden commit
HEAD~2      # dwa commity w rebase okienku
```

## Usuwanie gałęzi 'remote/feature/task' z repozytorium.

usuwamy gałąź `'feature/task'` lokalnie (opcjonalne)

`$ git branch -d feature/task`

usuwamy gałąź `'feature/task'` z repozytorium

`$ git push --delete origin feature/task`

aktualizujemy inne lokalne repozytoria

`$ git fetch --prune origin`

lub

`$ git remote prune origin`

## Tagowanie gałęzi 'master'

tagowanie
```bash
On branch master
$ git tag v0.1
```
wysyłanie zmian wraz z etykietami
```bash
On branch master
$ git push --tags origin master
```
pobieranie zmian wraz z etykietami
```bash
On branch master
$ git pull --tags origin master
```

# GiT useful commands

[Return](../../README.md) to main page.

## Ignoring files
If we need to ignore file or files temporarily, we can do this in the simple way!
We can do this using command
```bash
git update-index --assume-unchanged <path_to_file>
```
But remeber... If you go to the older commit or rebase your branch, git **automatically reset** this flag!

### Undo
If file has set unchanged flag, we can undo this using
```bash
git update-index --no-assume-unchanged <path_to_file>
```
Now git tracking this file again!

### File list
Files with `h` flag are hidden and similar with `H` flag are visible for git.
```bash
git ls-files -v | grep '^h'
```

### Addition
Alternative command working similar to --assume-unchanged is
```bash
git update-index --skip-worktree <path_to_file>
```